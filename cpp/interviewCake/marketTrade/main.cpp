/*
 * Author: M. Akif Oezkan
 * Solution: O(n) time and O(1) space
 *
 * Source : https://www.interviewcake.com
 * Sample interview question
 *
 * Suppose we could access yesterday's stock prices as a list, where:
 * The indices are the time in minutes past trade opening time, which was 
 * 9:30am local time. The values are the price in dollars of Apple stock at 
 * that time. So if the stock cost $500 at 10:30am, 
 * stock_prices_yesterday[60] = 500.
 *
 * Write an efficient function that takes stock_prices_yesterday and returns
 * the best profit I could have made from 1 purchase and 1 sale of 1 Apple 
 * stock yesterday.
 *
 * For example
 *   stock_prices_yesterday = [10, 7, 5, 8, 11, 9]
 *
 *   get_max_profit(stock_prices_yesterday)
 *   # returns 6 (buying for $5 and selling for $11)
 *
 *   No "shorting"—you must buy before you sell. You may not buy and sell in 
 *   the same time step (at least 1 minute must pass).
 */
#include <iostream>
#include <cstdlib>
#include <ctime>


void printArry(int* prices, size_t count){ 

  for (size_t i = 0; i < count; i++) {
    std::cout << prices[i] << "\t";
  }
  std::cout << std::endl;
}

void get_max_profit(int prices[], size_t count){ 

  int buy = prices[0];
  int sale = prices[1];
  int profit = sale - buy;
  int min = (profit < 0) ? sale : buy;

  for (size_t i = 2; i < count; i++) {
    int next_price = prices[i];

    // The "next" facilitates a better profit with the "min":
    if(next_price - min > profit){
      sale = next_price;
      buy = min; 
      profit = sale - buy;
    }

    // The "next" is smaller than the "min"
    // -> update the "min"
    if(next_price < min){
      min = next_price;
    }
  }
  
  // Print the results
  std::cout << "buy = " << buy << " \t" << "sale = " << sale << std::endl; 
  std::cout << "Maximum possible profit = " << profit << std::endl; 
}


int main(int argc, const char *argv[]) {
  
  const size_t count = 10;
  int stock_prices_yesterday[count];

  // Randomly initialize prices and print
  std::srand(std::time(0));
  for(size_t i = 0; i < count; i++){
    stock_prices_yesterday[i] = std::rand() % 64; 
  } 

  // Function call
  printArry(stock_prices_yesterday, count);
  get_max_profit(stock_prices_yesterday, count); 

  // Initialize prices
  //int a[count] = {50,  12,  47,  19,  49,  49,  60,  8, 22,  51};
  //printArry(a, count);
  //get_max_profit(a, count); 

  return 0;
}
